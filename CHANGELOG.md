## [Unreleased]

### Added

### Changed

### Fixed



## [2.13.02] - 2020-06-12 - 08:13

### Added
 - [BAPITEC1-99] - STA: Ajouter l'enseigne BPCE dans les cibles ICG de l'IDP BEL_STA
 - [BAPITEC1-100] - Rendre disponible l'IDP BEL_STA (ICG avec STA) sur OpenAPI

### Changed

### Fixed



## [2.13.01] - 2020-06-08 - 15:13

### Added
 - [BAPITEC1-75] - STA : prise en compte des enseignes BDP et BMA
 - [BAPITEC1-74] Ajout IDP PARTENAIRES_REST_16188 et IDP TYPE PARTENAIRES (ICG) pour le flux ROPC

### Changed

### Fixed



## [2.13.00] - 2020-06-04 - 07:45

### Added
 - [BAPITEC1-76] - Mise en place du flow JWT Authorization Grant pour les partenaires

### Changed

### Fixed



## [2.12.02] - 2020-05-27 - 10:51

### Added

### Changed
 - [BAPIDST-XXXX] - Mise à jour certificat Prod.Listeners - Ajout des SAN as-part-ath-groupe.f.bbg & rs-part-ath-groupe.f.bbg
 
### Fixed



## [2.12.01] - 2020-05-25 - 15:24

### Added

### Changed
 - [BAPIDST-XXXX] - Mise à jour certificat listeners PIC_INFRA

### Fixed

### Performance
 - [BAPITEC3-129] - Optimisations Cassandra : désactivation du row cache pour les tables KPS type cache


## [2.12.00] - 2020-05-18 - 10:50

### Added
 - [BAPIDST-3917] - Renouvellement des certificats de signatures (clés publiques) GAAP Hors Prod & Prod
 - [BAPITEC1-84] - Ajout du login de connexion dans le claim "sess" du JWT aval
 - [BAPITEC1-82] - Ajout de l'idp de type Partenaire PART_BPCE_SA

### Changed

### Fixed
 - [BAPIDST-3848] - Réinitialiation du cache du mapping des logs events lors des déploiements
 - [BAPIDST-3857] - Retour HTTP 502 au lieu de 500 si backend indisponible
 - [BAPIDST-3955] - Exclusions messages d'erreur (faux positifs) lors du déploiement

### Performance
 - [BAPITEC3-134] - Optimisations Cassandra : conservation du maximum de données en mémoire
 - [BAPITEC3-133] - Optimisations Cassandra : réadaptation de la compaction
 - [BAPITEC3-132] - Optimisations Cassandra : désactivation de la compression
 - [BAPITEC3-129] - Optimisations Cassandra : activation du row cache

## [2.11.09]

### Added

### Changed
 - [BAPIDST-3901] - HOM Auth - Màj certificat listener pour prise en compte AS canal partenaire

### Fixed

### Performance
 - [BAPITEC3-67] - Désactivation du Real Time Monitoring pour l'ANM


## [2.11.08] - 2020-04-09 - 14:39

### Added

### Changed

### Fixed
 - [BAPIDST-3866] Correction de la génération du jeton aval pour les backends avec jwt_required à false


## [2.11.07] - 2020-04-08 - 15:49

### Added

### Changed
 - [BAPITEC3-63] - Prise en compte de la mutualisation des certificats sur plusieurs environnements

### Fixed


## [2.11.06] - 2020-04-08 - 09:57

### Added
 - [BAPIDST-3722] - AppImport : prise en compte de l'import des certificats dans les credentials

### Changed
 - [BAPITEC3-62] - Renouvellement Cert PROD avec ajout urls environnement PROD_STD_GRP_EXT

### Fixed



## [2.11.05] - 2020-04-02 - 13:01

### Added

### Changed

### Fixed
 - [BAPIDST-3829] - MAJ du certificat  d'authentification forte ROA WSP pour la Prépa Prod (correction du CN)



## [2.11.04] - 2020-03-31 - 13:39

### Added

### Changed

### Fixed
 - [BAPIDST-3825] - Correction de contrôle de bon format de l'entête spécifique authz basic


## [2.11.03] - 2020-03-31 - 09:32

### Added
 - [BAPIDST-XXXXX] - Ajout du certificat d'authentification forte ROA WSP pour la Prépa Prod

### Changed

### Fixed



## [2.11.02] - 2020-03-27 - 15:52

### Added
 - [BAPIDST-3795] - Prise en compte routage type AJNA

### Changed
 - [BAPIDST-3800] - Surcharge du detail pour les erreurs sur timeout du backend
 - [BAPIDST-3803] - Logs events (suppressions des champs 'login_hint' , 'user.id' et 'session.user.id')

### Fixed



## [2.11.01] - 2020-03-20 - 13:38

### Added

### Changed

### Performance
 - [BAPIDST-3707] - Optimisation paramétrage ANM - (conformément à la suite des travaux BAPIDST-3560)

### Fixed
 - [BAPIDST-3751] - Fix la décalaration de l'idp MOCK_IDP_TTS en HOM_89C3


## [2.11.00] - 2020-03-11 - 11:02

### Added

### Changed
- [BAPIDST-3573] - DEI - Récupération de cookies ASP.Net "FormsAuthMobile"

### Fixed
- [BAPIDST-3723] Ajout du drift SAML sur le service OAuth consent


## [2.10.09] - 2020-03-13 - 11:11

### Added

### Changed

### Fixed
 - [BAPIDST-3767] - Prise en compte de la connexion vers WSBAD via proxy (bypass proxy = false)


## [2.10.08] - 2020-03-10 - 13:07

### Added

### Changed

### Fixed


## [2.10.07] - 2020-02-28 - 13:22

### Added

### Changed

### Fixed
 - [BAPIDST-3688] - Fix DSP2 Contrôles QWAC sur flow refresh_token


## [2.10.06] - 2020-02-27 - 18:51

### Added

### Changed
 - [BAPIDST-3695] - Màj Certificat Signature SAML GAAP_TTS - Hors-Prod 

### Fixed



## [2.10.05] - 2020-02-26 - 09:52

### Added
 - [BAPIDST-1530] - Ajout policy de promotion d'API pour appliquer les param�tres CORS

### Changed

### Fixed
 - [BAPIDST-3676] - Fix Check Bypass Proxy Config



## [2.10.04] - 2020-02-24 - 19:20

### Added
 - [BAPIDST-3003] - Ajouts identifiants de correlation des actes et sessions (X-BPCE-CorrelationId et X-BPCE-SessionId)
 - [BAPIDST-3648] - Ajout logs de l'ent�te X-Sonde dans les event logs (appels RS)

### Changed
 - [BAPIDST-3560] - Externalisation de l'activation/d�sactivation du traffic monitor (ANM)

### Fixed
 - [BAPIDST-3659] [BAPIDST-3662] Fix SSO IDP WSBAD


## [2.10.03] - 2020-02-20 - 14:45

### Added
 - [BAPIDST-2229] Ajout du pattern d'erreur BAPI pour les messages d'erreurs gerees par la Gateway
 - [BAPIDST-2824] Ajout des Certificats pour l'environnement SANDBOX_ISW
 
### Changed
 - [BAPIDST-2686] - Externalisation mode maintenance des instances (healthchecklb)

### Fixed



## [2.10.02] - 2020-02-14 - 10:37

### Added

### Changed
 - [BAPIDST-2932] - Customisation de l'URL WSBAD en fonction de l'usage ou non du proxy
 - [BAPIDST-1920] - Ajout de l'utilisateur authentifi� (cs.session.userid) dans les event logs

### Fixed
 - [BAPIDST-3046] Fix OAuth Token authorization_code erreur HTTP 403 (et HTTP 500 sur code inconnu) par HTTP 400 invalid_client (et HTTP 400 invalid_grant)
 - [BAPIDST-2965] - Custom Event Logs : prise en compte de l'override


## [2.10.01] - 2020-02-07 - 16:49

### Added
 - [BAPIDST-2964] Authentification forte - utilisation du claim sess.sub (si pr�sent) pour valorisation du NameID lors de l'utilisation du header X-StepUp-AuthN-Hint

### Changed

### Fixed



## [2.10.00] - 2020-02-06 - 17:48

### Added

### Changed

### Fixed
 - [BAPIDST-2981] - Prise en compte du drift sur les paramètres AuthInstant et SessionNotOnOrAfter de la réponse SAML


## [2.09.04] - 2020-02-04 - 15:20

### Added
 
### Changed
 - [BAPIDST-2997] D�sactivation de l'alimentation du claim sess.apidp en retour ICG (R/A BAPIDST-1754)

### Fixed



## [2.09.03] - 2020-01-28 - 16:43

### Added
 - [BAPIDST-2922] - Ajout des issuers TIERS pour les applications TIERS (ICG)
 - [BAPIDST-2831] - Int�gration certificats (Listeners + Signature JWT) - PIC_PERF
 - [BAPIDST-2962] - Int�gration certificats (Listeners + Signature JWT) - HOM_ANO_BIS
 
### Changed

### Fixed
 - [BAPIDST-2972] - Correction du routage par exception et par m�thode


## [2.09.02] - 2020-01-22 - 16:12

### Added
 - [BAPIDST-2806] Ajout de l'IDP MOCK_IDP_TTS
 
### Changed

### Fixed


## [2.09.01] - 2020-01-20 - 08:38

### Added
 - [BAPIDST-2904] - Ajout des issuers ICG pour Palatine (40978)
 - [BAPIDST-2790] - Ajout du routage par exception par nom de méthode

### Changed
 - [BAPIDST-1247] - Suppression des IDP ICG qui ne sont plus utilisés
 
### Fixed


## [2.09.00] - 2020-01-08 - 10:16

### Added
 - [BAPIDST-2792] - Event_logger : Ajout de champs AS (login_hint)
 - [BAPIDST-1754] - Alimentation du claim sess.apidp avec l'issuer des AuthNRequest pour ICG(sta), ICGBAD, ICGIBP (cyberplus*,...)


### Changed

 
### Fixed
 - [BAPIDST-2809] - Proxy AxwayManager - Correction cloisonnement pour les appels sur le canal CC_WEBAPI
 

## [2.08.04] - 2020-01-23 - 16:22

### Added
 - [BAPIDST-2933] - Ajout d'un cache Event_logs_CS_Attrs
 
### Changed

### Fixed
 - [BAPIDST-2933] - Optimisation de la policy Process Event Logs


## [2.08.03] - 2020-01-13 - 10:10

### Added
 - [BAPIDST-2845] Valorisation du claim sub et sess.sub du jwt aval avec l'attribut BPCE-TraceID retourné par les IDP de type ICG, ICGBEL et ICGIBP

### Changed

### Fixed
 - [BAPIDST-2861] - Déproxification WSBAD


## [2.08.02] - 2019-12-20 - 18:04

### Added
 - [BAPIDST-2734] - Déclarations des IDPs pour le StepUp (BEL_STA)
 - [BAPIDST-2733] - Ajout des certificats pour l'IDP PROSPECT_EDP (homologation, préprod & production)
 - [BAPIDST-2639] - Ajout des IDP GAAP_FFZ et GAAP_CLI_FFZ

### Changed

### Fixed



## [2.08.01] - 2019-12-13 - 14:57

### Added
 - [BAPIDST-2721] - DEI - Récupération de cookies ASP.Net "FormsAuth"
 - [BAPIDST-2681] - Ajout cs_appname dans les logs event pour tous les cas + externalisation de la déclaration des mappings

### Changed

### Fixed
 - [PRB0003654] - Gestion des timeouts lors de l'appel des backends fournisseurs de services
 

## [2.08.00] - 2019-12-11 - 12:02

### Added
 - [BAPIDST-2538] DSP2 - Controle QWAC AISP (/token authorization_code) et debrayabilité

### Changed

### Fixed


## [2.07.06] - 2019-12-03 - 13:32

### Added

### Changed

### Fixed
 - [BAPIDST-2740] SwagImport - Retour arrière sur gestion des scopes par méthode


## [2.07.05] - 2019-12-02 - 13:42

### Added

### Changed

### Fixed
 - [BAPIDST-2732] SwagImport - Correction de la gestion d'erreur quand la liste des scopes d'une méthode est vide.


## [2.07.04] - 2019-11-28 - 19:32

### Added

### Changed

### Fixed
 - [BAPIDST-2720] - Relai du StepUp IDP ID lors de l'appel /api/oauth/v2/consume dans le cadre du STA


## [2.07.03] - 2019-11-27 - 16:53

### Added

### Changed

### Fixed
 - [BAPIDST-2677] fix plantage RS lors du traitement AuthNRequest (removeAttribute "ID") pour le stepup 
 - [BAPIDST-2674] - Correction gestion StepUp pour STA (multi-enseignes)


## [2.07.02] - 2019-11-22 - 15:13

### Added

### Changed

### Fixed
 - [BAPIDST-2675] - DEI - Fixer l'enseigne par défaut



## [2.07.01] - 2019-11-20 - 11:02

### Added

### Changed
 - [BAPIDST-1740] Modification du SwagImport pour créer des securityProfiles par méthode

### Fixed



## [2.07.00] - 2019-11-19 - 12:46

### Added
 - [BAPIDST-1831] - Déploiement policies : prise en compte paramétrages tables Cassandra
 - [BAPIDST-2103] - Gérer les enseignes dans l'IDP DEI

### Changed
 
### Fixed
 - [BAPIDST-2603] DSP2 - Cinématique PISP - traitement retour IDP GAAP stepup RS consume 

## [2.06.04]

### Added

### Changed
 
### Fixed
 - [BAPIDST-2576] - Mise a jour Certificat signature et chiffrement ICG Cyberplus PRE_ANO (Correction Report)


## [2.06.03]

### Added
 - [BAPIDST-2576] - Mise a jour Certificat signature et chiffrement ICG Cyberplus PRE_ANO

### Changed
 - [BAPIDST-2575] - Integration Certificat vip AS RS PRE_ANO avec nouveau SAN

### Fixed


## [2.06.02] - 2019-10-22 - 10:51

### Added
 - [BAPIDST-1866] - DSP2 - Ajout certificats dédiés à la signature de templates SAML

### Changed
 - [BAPIDST-2522] - Suppression des extensions pour la SAML AuthN Request GAAP
 - [BAPIDST-2424] - Suppression des certificats ADFS expirés

### Fixed



## [2.06.01] - 2019-10-15 - 11:36

### Added
 - [BAPIDST-2486] - GAAP_TTS - Ajout le certificat federation-signing pour hors prod 

### Changed

### Fixed



## [2.06.00] - 2019-10-08 - 14:50

### Added

### Changed
 - [BAPIDST-2031] - Débrayabilité du contrôle du cloisonnement RS
 - [BAPIDST-2239] - Cloisonnement des canaux coté RS en se basant sur http.request.url

### Fixed
 - [BAPIDST-2292] - Canal partenaire - Ajout de deux claims "ptid" et "ptcdetab" dans le jeton aval 

## [2.05.18]

### Added
 - [BAPIDST-2575] - Integration Certificat vip AS RS PRE_ANO avec nouveau SAN
 - [BAPIDST-2576] - Mise a jour Certificat signature et chiffrement ICG Cyberplus PRE_ANO
 

### Changed

### Fixed


## [2.05.17] - 2019-09-26 - 16:20

### Added
 - [BAPIDST-2424] - Mise à jour des certificats de signature et chiffrement ADFS BP et BPCE IT
 
### Changed

### Fixed
 - [BAPIDST-2258] - Fix Bug Set default dacs.SecurityLevel = 101 for CYBERPLUS-Lxyyy

## [2.05.16] - 2019-09-25 - 11:04

### Added
 - [BAPIDST-2402] - Arrimage BAD - Ajouter les Issuers manquants

### Changed

### Fixed


## [2.05.15] - 2019-09-20 - 16:40

### Added

### Changed

### Fixed
 - [BAPIDST-2375] - Fix dacs.SecurityLevel : Doit être 0 pour les PROSPECT.


## [2.05.14] - 2019-09-20 - 15:49

### Added

### Changed

### Fixed
 - [BAPIDST-2386] - Fix Bug Groovy Script : Prepare Required Keys To Use For Signature - No ContentBody on GET request


## [2.05.13] - 2019-09-20 - 13:24

### Added

### Changed
 - [BAPIDST-2374] - Mettre à jour les certificats et les urls PROD  GAAP NWM + TTS

### Fixed
 - [BAPIDST-2380] - Arrimage BAD ICG - Fixer l'erreur dans le formatage 


## [2.05.12] - 2019-09-16 - 15:27

### Added

### Changed
 - [BAPIDST-2258] - ICG  - Modification du dacs.SecurityLevel par défaut à 101 (Suite Revert du 05/09/19)
 - [BAPIDST-2256] - Suppression du contrôle de validité de l'authority DEI
 
### Fixed



## [2.05.11] - 2019-09-13 - 17:09

### Added
 - [BAPIDST-2278] - Appels interservices: transfert de l'objet origin si présent dans le jeton upstream
 - [BAPIDST-2162] - Implémentation GAAP_NWM (GAAP)

### Changed
 - [BAPIDST-2276] - STA - ICG - Regroupement de toutes les enseignes sur une même déclaration d'IDP + utilisation de l'entête Accept='application/xml'

### Fixed


## [2.05.10] - 2019-09-12 - 07:26

### Added

### Changed
 - [BAPIDST-2322] - Désactivation logs traffics

### Fixed


## [2.05.09] - 2019-09-11 - 18:01

### Added
 - [BAPIDST-2264] - DSP2 - Ajout du scope offline_access dans le consentement profile

### Changed
 - [BAPIST-2291] - Débrayabilité de la signature HTTP dans le contexte DSP2 + bypass signature si certificat absent

### Fixed


## [2.05.08] - 2019-09-06 - 18:10

### Added

### Changed
 - [BAPIDST-2276] - STA - ICG - Regroupement de toutes les enseignes sur une même déclaration d'IDP

### Fixed



## [2.05.07] - 2019-09-06 - 17:58

### Added

### Changed

### Fixed


## [2.05.06] - 2019-09-06 - 14:28

### Added

### Changed

### Fixed
 - [BAPIDST-2294] - Correction de la configuration des caches (DiskPersistence vs Eviction LFU)  (Compatibilité SP11)

## [2.05.05] - 2019-09-05 - 11:11

### Added
 - [BAPIDST-2272] - Ajout des certificats QSealC_Sign.prod.30007 & QSealC_Sign.prod.18919

### Changed
 - [BAPIDST-2266] - Modification de la méthode de vérification du status de l'instance après son démarrage

### Fixed
 - [BAPIDST-2204] - Appels inter services - désactivation de l'anti rejeu sur le jeton X-Upstream-JWT-Token
 - [BAPIDST-2265] - DSP2 Correction des erreurs rémontées en cas d'erreur de Signature des réponses

## [2.05.04] - 2019-09-02 - 11:17

### Added
 - [BAPIDST-2182] DSP2 - Ajout des certificats de validation de signature JWT Natixis 89C3 (Stepup)

### Changed
 - [BAPIDST-2257] - DSP2 - Vérification signature HTTP (suppression du double contrôle des headers)
 - [BAPIDST-2258] - ICG  - Modification du dacs.SecurityLevel par défaut à 101

### Fixed
 - [BAPIDST-2255] - Gestion de l'absence du certificat QSealC dans la cinématique AISP

## [2.05.03] - 2019-08-28 - 10:41

### Added
 - [BAPIDST-1248] - Utilisation service STA (Négociation SAML Externe) pour les nouveaux IDP ICG
 - [BAPIDST-2060] - DSP2 - Signature HTTP des réponses avec les certificats QSealC ASPSP
 - [BAPIDST-1618] - DSP2 - Vérification de la signature HTTP des requêtes

### Changed

### Fixed
 - [BAPIDST-2137] - Correction du script Generate State Object pour mémoriser le request issuer dans le relay state

## [2.05.02] - 2019-08-13 - 14:09

### Added
 - [BAPIDST-2193] - Ajout mécanisme de mise en cache des opérations de redémarrage et déploiement .fed

### Changed

### Fixed
 - [BAPIDST-2192] - Correction export .dat API suite à changement sur SP11
 - [BAPIDST-2184] - Correction de controles non souhaités sur certains appel d'API (mediaType)
 
## [2.05.01] - 2019-08-08 - 16:12

### Added

### Changed

### Fixed
 - [BAPIDST-2178] - Correction de la définition de la table KPS de cache nonce AS

## [2.05.00] - 2019-08-07 - 09:01

### Added
 - [BAPIDST-1320] Ajout table KPS pour les certificats QSEALC
 - [BAPIDST-1923] DSP2 PISP Anti rejeu - Ajout du nonce au format application/json
 - [BAPIDST-2119] - AppImport - Prise en compte flag "skipHttpSigVerification""
 - [BAPIDST-2127] - AppImport - Prise en compte flag "disableConsentProfile"

### Changed
 - [BAPIDST-70] - Non rejeu sur les RelayState d'Authentification (pas sur le consentement)
 - [BAPIDST-1829] - JWT - Valorisation du champ "sub" via SamAccountName
 - [BAPIDST-2086] - Suppression code IPBan (mise en conformité code policies)

### Fixed
 - [BAPIDST-2050] - Correction processus de mise à jour (ajout / suppression) de données dans les tables KPS
 - [BAPIDST-2055] - Fix OIDC format du paramètres claims (id_token et/ou userinfo)

## [2.04.11] - 2019-08-27 - 21:47

### Added

### Changed

### Fixed
 - [BAPIDST-2044] DSP2 - Stepup RS X-ConsumePath - fix report 2.04 correctif de la valeur du chan dans le jeton aval

## [2.04.10] - 2019-08-21 - 18:03

### Added
 - [BAPIDST-2044] Appels inter service - transfert de l'objet PSD2 dans le jeton aval

### Changed

### Fixed
 - [BAPIDST-2044] DSP2 - Stepup RS X-ConsumePath - correctif de la valeur du chan dans le jeton aval

## [2.04.09] - 2019-07-29 : 13:29

### Added
 - [BAPIDST-2128] - Déclaration / autorisation profil de scope DSP2 pour les consentements

### Changed
 - [BAPIDST-2124] Mise à jour du script de déploiement pour ignorer une erreur non pertinente apparaissant avec le SP11

### Fixed


## [2.04.08] - 2019-07-26 : 14:55

### Added

### Changed
 - [BAPIDST-2118] - Suppression module incompatible avec SP11

### Fixed


## [2.04.07] - 2019-07-26 : 13:55

### Added

### Changed
 - [BAPIDST-2088] - Suppression du patch "oauth-consent"
 - [BAPIDST-2089] - Suppression du patch "selectors"

### Fixed


## [2.04.06] - 2019-07-23 : 16:28

### Added

### Changed

### Fixed
 - [BAPIDST-2062] FIX DSP2 AISP authent primaire en stepup / problème X-StepUp-IDPID 
 - [BAPIDST-2058] FIX DSP2 AISP authent primaire en stepup / scopes manquants dans l'objet request

## [2.04.05] - 2019-07-16 - 16:20

### Added

### Changed
 - [BAPIDST-1974] - Ajout du log pour le controle du canal RS

### Fixed
 - [BAPIDST-2028] - Correction problème de concurrence lors de la création des tables KPS
 - [BAPIDST-2016] - Fix issue : don't follow redirect on Consume Services
 - [BAPIDST-2017] - fix DSP2 ajout endpoints stet consume et consent (AS)

## [2.04.04] - 2019-07-08 - 12:00

### Added

### Changed
 - [BAPIDST-1973] Désactivation vérification SNI

### Fixed


## [2.04.03] - 2019-07-04 - 15:30

### Added

### Changed

### Fixed
 - [BAPIDST-1958] - DSP2 Fix du stepup consume RS pour l'anti rejeu X-Nonce


## [2.04.02] - 2019-07-01 - 14:50

### Added

### Changed

### Fixed
 - [BAPIDST-1944] - Fix sur la version de l'extension emabrquant la suppression de la gestion du champ "clientSecretDerogation"

 
## [2.04.01] - 2019-07-01 - 10:10

### Added

### Changed

### Fixed
 - [BAPIDST-1918] - Accès RS EX ATH DEV
 - [BAPIDST-1846] - Fix Init providerID - Débrayage/paramétrage du bypass proxy lors des appels vers les IDP

 
## [2.04.00] - 2019-06-21 - 11:56

### Added
 - [BAPIDST-1626] Activation des logs traffic
 - [BAPIDST-49] - DSP2 : Intégration IDP GAAP TTS 
 - [BAPIDST-979] - Extension du mécanisme de désactivation du consentement
 - [BAPIDST-1542] - Implémentation MOCK_IDP (DEVTEST)

### Changed
 - [BAPIDST-1491] Renommage du champ disablingMixedMode en allowApisScopes
 - [BAPIDST-1613] Suppression du flag derogation client_secret
 - [BAPIDST-894] - Modification Jeton Aval v4 : ajout claim du matricule du conseiller 
 
### Fixed
- [BAPIDST-1463] - Cloisonnement logique des canaux (AS + RS)
- [BAPIDST-1288] - Utilisation de la version OAuth v2 sur les endpoints DSP2 : /stet/psd2/oauth/(token|authorize)


## [2.03.17] - 2019-07-23 : 11:18

### Added
 - [BAPIDST-1985] - Déclaration des certificats de PRE_ANO (Pré Production Anonyme)

### Changed
 
### Fixed

## [2.03.16]

### Added

### Changed
 
### Fixed
 - [BAPIDST-2015] - Augmentation du timeout des transactions pour permettre le redémarrage des instances

## [2.03.15]

### Added
 - [BAPIDST-XXXX] Ajout des nouvelles AC GlobalSign

### Changed
 
### Fixed

## [2.03.14]

### Added
 - [BAPIDST-XXXX] Ajout des certificats pour HOM Bis
 
### Changed
 
### Fixed
 - [BAPIDST-XXXX] Suppression du Bypass proxy pour DEI en Production
 
 ## [2.03.13]

### Added

### Changed
 
### Fixed
 - [BAPIDST-1925] Bypass proxy pour DEI sans passer par la table KPS


## [2.03.12]

### Added
 - [BAPIDST-213] Ajout Certificats pour environnements HOM_BIS
 - [BAPIDST-1305] Ajout Certificats pour environnement PRE

### Changed
 
### Fixed

## [2.03.11]

### Added

### Changed
 
### Fixed
 - [BAPIDST-1883] Ajout d'un timeout sur le démarrage des instances lors d'un déploiement de policies

## [2.03.10]

### Added

### Changed
 - [BAPIDST-1846] - Débrayage/paramétrage du bypass proxy lors des appels vers les IDP
 
### Fixed
 - [BAPIDST-1463] - Cloisonnement logique des canaux (AS + RS)
 - [BAPIDST-1288] - Utilisation de la version OAuth v2 sur les endpoints DSP2 : /stet/psd2/oauth/(token|authorize)

## [2.03.10]

### Added

### Changed
 - [BAPIDST-1846] - Débrayage/paramétrage du bypass proxy lors des appels vers les IDP
 
### Fixed

## [2.03.09]

### Added

### Changed
 
### Fixed
 - [BAPIDST-1793] - Relais du nom de l'application dans le contexte StepUp

## [2.03.07]

### Added
 - [BAPIDST-1052] - Ajout d'un IDP Partenaire Natixis Assurances / BPCEA
 - [BAPIDST-1697] - Ajout de l'url du template Saml pour l'IDP Accreditation CBPII

### Changed
 - [BAPIDST-1697] - Changement de l'url du template Saml pour l'IDP Accreditation AISP
 - [BAPIDST-1051] - Ajout du cdetab dans le RelayState SAML
 - [BAPIDST-1051] - Ajout des informations partenaires dans le jeton aval v5
 
### Fixed

## [2.03.06]

### Added

### Changed
 - [BAPIDST-1370] - Remplacement de l'utilisation de ehCache par Cassandra
 - [BAPIDST-1604] - Bypass proxy sur les communications AS -> DEI

### Fixed


## [2.3.5] - 2019-06-05 - 14:00

### Added

### Changed
 - [BAPIDST-1370] - Remplacement de l'utilisation de ehCache par Cassandra
 - [BAPIDST-1604] - Bypass proxy sur les communications AS -> DEI
 
### Fixed
 - [BAPIDST-1782] - Correction IDP ICGBEL - Set ICG Extensions - Suppression double quote en trop
 - [BAPIDST-1781] - Correction IDP ICGBEL - Set ICG Extensions - Remplacement caractères d'échappement pour texte_sms

 
## [2.3.4] - 2019-05-22 - 10:00

### Added

### Changed
 - [BAPIDST-1727] - Optimisation des accès KPS
 
### Fixed


## [2.3.3] - 2019-05-16 - 11:14

### Added

### Changed
 
### Fixed
 - [BAPIDST-1712] - Suppression de la mention CRYPTE pour les appels mobiles en PART
 - [BAPIDST-1693] - Problème URL retour de la requête SAML lié à l'absence de redirect_uri

 
## [2.3.2] - 2019-04-29 - 13:05

### Added
 
### Changed
 - [BAPIDST-766] - Suppression données CAP et ajout niveau de sécurité pour type beta
 - [BAPIDST-766] - Définition de l'issuer pour le niveau de sécurité de type beta
 - [BAPIDST-766] - Ajout mention CRYPTE pour les appels mobiles en PART

### Fixed


## [2.3.1] - 2019-04-16 - 16:28

### Added
 - [BAPIDST-1118] - Ajout proxy API Manager accessible via RS (API axwayManager)
 - [BAPIDST-828] Ajout du endpoint d'info sur les exceptions de consentements : /info/consentexception

### Changed
 - [BAPIDST-1299] - Prise en compte du header X-CdEtab envoyé par le F5 sur l'AS (services OAuth2)

### Fixed



## [2.3.0] - 2019-04-10 - 17:18

### Added

### Changed
 - [BAPIDST-1469] - DSP2 : Désactivation du contrôle du Certificat QWAC en Try It

### Fixed

## [2.2.10] - 2019-04-09 - 14:44

### Added

### Changed

### Fixed
 - [BAPIDST-1586] - Correction Set ICG Extensions - CE - inversion dacs:SecurityLevel pour PRO en mode REST
 - [BAPIDST-1587] - Fix bug sur le grant des APIs pour les TPPs

## [2.2.9]

### Added

### Changed

### Fixed
 - [BAPIDST-1570] Ajout du grant pour les tpp et grant all sur les API Open

## [2.2.8]

### Added

### Changed

### Fixed
 - [BAPIDST-1529] Endpoint info APIs : Fix sur le filtre des cannaux
 - [BAPIDST-1578] Positionnement du champ useClientRegistry à false


## [2.2.7] - 2019-03-29 - 10:43

### Added

### Changed

### Fixed
 - [BAPIDST-766] - Fix Bug AuthenticationFactor attribute Type : need to start with uppercase


## [2.2.6] - 2019-03-28 - 19:28

### Added

### Changed

### Fixed
 - [BAPIDST-766] - Fix Bug AuthenticationFactor attribute


## [2.2.5] - 2019-03-26 - 17:27

### Added
 - [BAPIDST-1455] - Ajout des IDPs WSBADCCOOP & ICGCCOOP

### Changed
 - [BAPIDST-889] Sécurité entrante : Prise en compte de Template Request et Anti Replay sur l'import des Swaggers

### Fixed
 - [BAPIDST-766] Fix Bug : bpcesta.typ_act = sso don't have to generate dacs:enrollment node
 - [BAPIDST-1485] - Webapi - Désactivation WAF suite à lenteurs observées
 - [XXX] Fix bug in Scripting Language : Validate Saml Parameters - Set binding to lower case
 - [BAPIDST-1495] - Désactivation de la validation du cache nonce (IDP local)
 - [BAPIDST-1488] - Retour erreur 500 si erreurr lors de la mise en cache du RelayState lors d'une requête authorize

## [2.2.4] - 2019-03-19 - 22:59

### Added
 - [BAPIDST-1197] - Ajout response_mode=form_post sur les requêtes AS
 - [BAPIDST-179] - Ajout du header Strict-Transport-Security sur les réponses HTTP

### Changed

### Fixed
 - [BAPIDST-1411] - [BAPIDST-1419] - Fix appels interservice sur claim auth_time
 - [BAPIDST-1433] - Fix Bug Validate DEI authority
 - [BAPIDST-766] - Fix bug : set dacs:securityLevel + encode saml request in renew mode

## [2.2.3] - 2019-03-15 - 10:06

### Added
 - [BAPIDST-1262] - Déclaration IDP Template --> APPACCRE

### Changed

### Fixed
 - [BAPIDST-766] - Fix bug retrieving query param display + add new ICG issuers



## [2.2.2] - 2019-03-13 - 16:49

### Added
 - [BAPIDST-494] DSP2 - Récupération de l'empreinte du certif QWAC et contrôle du clientId sur /token
 - [BAPIDST-766] Arrimage BAD - Déclaration de 9 nouveaux IDPs : BEL_CE, BEL_BP, BEL_BTP, BEL_CFF, BEL_CCOOP, BEL_BCP, BEL_BQSAV, BEL_CM et BEL_BQPA
 
### Changed

### Fixed
 - [BAPIDST-1292] - Déploiement policies: ajout de nouveaux messages d'erreurs dans la whitelist


## [2.2.1] - 2019-03-06 - 18:41

### Added
 - [BAPIDST-1178] - Prise en compte du champs AUTHORITY dans l'assertion si présent

### Changed
 - [BAPIDST-838] - Externalisation de la configuration app.config

### Fixed
 - [BAPIDST-1169] - Suppression de la session OAuth (correctif)
 - [BAPIDST-1166] - Correction timeout sur endpoints infos APIs et APPs
 - [BAPIDST-1253] - Correction sur StepUp RS avec utilisation X-StepUp-AuthN-Hint (DSP2 cinématique PISP)

## [2.2.0] - 2019-02-15 - 11:32

### Added
 - [BAPIDST-594] - Log du header X-BPCEClientContext et du userid (claim sub du JWT)
 - [BAPIDST-995] - Ajout description sur toutes les tables KPS (suite et fin)
 - [BAPIDST-1005] - Upgrade SDK
 - [BAPIDST-891] DSP2 - PISP - Création anti rejeu RS
 - [BAPIDST-893] DSP2 - PISP - Contrôle anti rejeu RS
 - [BAPIDST-1059] - Création des endpoints DSP2 AS
 - [BAPIDST-1129] - Gestion du cookie CEMOBBANKING sur les appels WSBAD
 
### Changed
 - [BAPIDST-892] - Mise à jour module Service BPCE (pre-commit AISP)
 - [BAPIDST-1010] - Déplacement de l'alimentation de cs.channel dans la policy Customize Event Log
 - [BAPIDST-1169] - Suppression de la session OAuth
 
### Fixed


## [2.1.5] - 2019-01-30 - 19:24

### Added

### Changed

### Fixed
 - [BAPIDST-998] - Remise en place du délai à prendre entre l'arrêt et la relance d'une instance

## [2.1.4] - 2019-01-30 - 18:38

### Added

### Changed

### Fixed
 - [BAPIDST-997] - Le redémarrage des instances n'était pas encore parallélisé (oubli de commit de script)


## [2.1.3] - 2019-01-30 - 16:23

### Added
 - [BAPIDST-594] - Log du header X-BPCEClientContext

### Changed
 - [BAPIDST-989] - Déploiement des policies: ajout délai entre l'arrêt et la relance d'une instance
 - [BAPIDST-434] - Healthcheck - Suppression de certains contrôles (port TCP + écriture) sur l'ehcache.
 - [BAPIDST-981] - Endpoints : Ajout du canal REBOND_DMZ
 - [BAPIDST-983] - Endpoints : Ajout du paramètre d'activation du filtre sur les organisations (permet de le désactiver sur les environnements 89C3)
 - [BAPIDST-995] - Traitement de l'idpspace pour systématiser la terminaison par /
 - [BAPIDST-995] - Ajout description sur toutes les tables KPS

### Fixed


## [2.1.2] - 2019-01-25 - 11:30

### Added
 - [BAPIDST-381] - Ajout du champ 'organizationIdentifier' dans le descripteur d'application consommatrice
 - [BAPIDST-383] - Ajout du champ custom pour DPS2 'organizationIdentifier' et gestion de l'import des APP DSP2
 - [BAPIDST-673] - Différenciation BP & CE pour les metadatas
 - [BAPIDST-902] - Alimentations des Tx Event Log Custom Attributes sur les cas d'erreur

### Changed
 - [BAPIDST-378] - Suppression de la table KPS Stepup_Session
 
### Fixed
 - [BAPIDST-673] - Personnalisation des SAML AuthN Request en fonction des IDPs POC Arrimage BAD


## [2.1.1] - 2019-01-15 - 13:14

### Added
 - [BAPIDST-673] - Ajout des IDP WEB_BP, MOB_BP, WEB_DEI_PART, WEB_DEI_PRO, BANXO_PART & BANXO_PRO
 - [BAPIDST-361] - Ajout du support de l'algo 2 pour le claim hash du jeton aval (v4)

### Changed

### Fixed



## [2.1.0] - 2019-01-10 - 13:46

### Added
 - [WSGAPI-99] DSP2 - Gestion Anti-Rejeu RS : ajout du cache distribué "AntiReplayNonce"
 - [WSGAPI-99] DSP2 - Gestion Anti-Rejeu RS : ajout des policy PSD2 Features > Anti Replay et Invoke Policy Security
 - [BAPIDST-67] DSP2 - Jeton V3 - ajout claim psd2
 - [BAPIDST-52] Jeton V3 - origins/dsp2/... (ajout du claim last_login pour IDP ICGIBP v2) + (modification version: v3)
 - [BAPIDST-55] - Ajout des entêtes X-Sonde, X-Forwarded-For et X-Forwarded-Port dans les Transactions Event Logs
 - [BAPIDST-220] Endpoint Info APIs : Ajout relativePath sur les methods
 - [BAPIDST-131] SwgaImport Servlet : Gestion des tags sur les frontends
 - [BAPIDST-368] - Ajout des certificats de la CA GlobalSign
 - [BAPIDST-359] - Déclaration des IDP sur la plateforme OpenAPI (89c3)
 - [BAPIDST-61] - DSP2 gestion SAML RS 
 - [BAPIDST-477] - Ignorer le warning sur la non résolution des CN des certificats lors du déploiement des policies
 - [BAPIDST-85] - Activation de la parallélisation du déploiement des policies sur les instances APIGateway 
 - [BAPIDST-472] - DSP2  AISP  010 Prise en charge du header X-StepUp-IDPID
 - [BAPIDST-744] - Ajout de la WAF (modsecurity)
 - [BAPIDST-360] - DSP2 : Ajout du claim psu_ip_address dans le jeton aval (v4)
 - [BAPIDST-546] - DSP2 : Ajout du claim nonce dans le jeton aval (v4)

### Changed
 - [BAPIDST-221] Endpoints Infos : Formattage des dates au format ISO-8601
 
### Fixed
  - [BAPIDST-191] Endpoints /info/apps : Prise en charge de l'erreur sur la présence d'un certificat incorrect dans les credentials

## [2.0.39] - 2019-01-10 - 8:30

### Added

### Changed

### Fixed
 - [BAPIDST-635] - Séparations des endpoints d'appels lors des appels ICG Prospects (BP, CE & FC)
 - [BAPIDST-785] - Routage par exception - Bug pour les applications sans exceptions

## [2.0.38] - 2018-12-11 - 13:59

### Added

### Changed

### Fixed
 - [BAPIDST-478] -  bug - method setOAuthError in OAuthParameters.java - missing iterator.next()
 - [BAPIDST-478] -  bug - method parseRedirectURIParameter from AuthorizeParameters.java

## [2.0.37] - 04 décembre 2018 à 15:11

### Added
 - [BAPIDST-239] - Implémentation IDP DEA

### Changed

### Fixed


## [2.0.36] - 03 décembre 2018 à 09:27

### Added

### Changed

### Fixed
 - [BAPIDST-431] - Déclaration et paramétrage des RemoteHost des IDP en mode statique


## [2.0.35] - 30 novembre 2018 à 12:40

### Added

### Changed
 - [BAPIDST-434] - Healthcheck - Suppression du controle des caches

### Fixed


## [2.0.34] - 22 novembre 2018 à 11:36

### Added
 - [BAPIDST-368] - Ajout des certificats de la CA GlobalSign

### Changed

### Fixed
 - [BAPIDST-350] - Correction des fqdn des instances BILMWSG016 et BILMWSG016


## [2.0.33] - 13 novembre 2018 à 13:57

### Added

### Changed

### Fixed
 - [BAPIDST-252] - Correction du pattern de validation des autorités DEI
 
## [2.0.32]

### Added

### Changed
 - [BAPIDST-195] - Application de la politique 'longest path match' sur le endpoint /healthcheck

### Fixed


## [2.0.31]

### Added
 - [BAPIDST-97] - Ajout du binding SOAP pour PROSPECT_CE en Homologation, utilisé par l'application de test Perf_IBP_00
 - [BAPIDST-97] - Ajout du SAML Issuer urn:dictao:dacs:ce, utilisé par l'application de test Perf_IBP_00

### Fixed
 - [BAPIDST-139] - Désactivation de la migration automatique de la table routing_audience_exception V3 --> V4


## [2.0.30]

### Fixed
 - [BAPIDST-51] - Homogénéisation du CORS sur les endpoints /api/oauth/*
                   Seul le header 'Authorization' est accepté à ce jour sur les endpoints oauth de l'AS

## [2.0.29]

### Fixed
 - [WSGAPI-96] - Fermeture / libération thread durant appel healthchecklb

## [2.0.28]

### Fixed
 - [WSGAPI-134] Fix comparaison NotBefore/NotOnOrAfter

## [2.0.27]

### Added

### Changed
 - [WSGAPI-134] - SAML Response - ajout d'un drift (tolérance) de 5 sec lors des validations des temps

### Fixed
 - [WSGAPI-90] - Restauration du FORCE HOST HEADER dans la policy WSP-Provider

## [2.0.26]

### Added

### Changed
 - [WSGAPI-131] - Modification de la gestion des proxies : envSettings au lieu de KPS
 
### Fixed
 - Correction Policy Saml Signer Validator : erreur de syntaxe dans le test de selection des certificats.


## [2.0.25]

### Added
 - [WSGAPI-133] - Homogénéisation du CORS sur les endpoints /api/oauth/*

### Fixed
 - [WSGAPI-96] - Sécurisation de l'appel au healthchecklb avec un timeout de 5 secondes
 - Correction des URLs (endpoints) des contextes PROSPECT ICG

## [2.0.24]

### Added
 - Ajout des serveurs manquants d'Homologation et Production dans InstanceSpecificConfig
 - Ajout du nouveau certificat ICG de signature des réponses SAML pour VFO (expire le 07/10/2018 à 10h44 !)

### Changed
 - Modification des certificats ROA CE HOM & PROD pour l'accès aux API sensibles NPS

## [2.0.23]

### Added
 - Ajout du certificat ROA BP pour l'accès aux API sensibles NPS

### Changed
 - [BAPI-252] SwagImport et AppImport : Changement de la sortie au format JSON.
 - [WSGAPI-111] Prise en charge du nouveau champ "Scopes en mode mixte" sur l'import d'application consommatrices.
 - [BAPI-251] Endpoints '/info/apis' et '/info/apps' : ajout d'un cache local pour accélérer le temps de réponse.
 - Modification du test d'environnement (Prod / Hors Prod) dans la policy Saml Signer Validator
 - Modification du signerDN pour ADFS CE VFO (Prise en compte du nouveau certificat)

## [2.0.22]

### Fixed
 - [WSGAPI-123] - Correction des paths (endpoints) exposés sur le port default services

## [2.0.21]

### Added
  - [WSGAPI-96] - Intégration de la vérification du cache distribué dans le healthchecklb
  - [WSGAPI-111] - Paramétrage du fonctionnement des scopes sur la déclaration d'application
  - Ajouts des certificats publique pour l'accès aux API sensibles Natixis pour les clients PFS et ROA
  - Ajout des certificats pour la Production Live 89C3 API
  - Ajout des certificats de signature pour l'IDP EDP BP de MOE et MOA

### Changed
  - Endpoint '/info/apps' : Ajout de l'identifiant de l'application et de la date de mise à jour dans le retour JSON.
  - Endpoint '/info/apis' : Ajout des Date mise à jour, Tags, Cors Profile, Security Profile et Methods dans le retour JSON.
  - Changement du niveau de log du RelayState
  - Servlet ApplicationImport : Gestion mise à jour des credentials

### Fixed
  - [WSGAPI-114] - Ajout de l'extraction du subject de l'ID Token (déclenche l'extraction du CDETAB)

## [2.0.20]

### Fixed
  - Suite oubli dans le report en 2.0.19 : 
  - [WSGAPI-97] - Relais du code établissement présent dans la session si non retourné par ICG

## [2.0.19]

### Fixed
  - Suite problème de report en 2.0.18 : 
  - [WSGAPI-97] - Relais du code établissement présent dans la session si non retourné par ICG 
  - Relais du claim 'opsId' dans une négociation StepUp (encodage/décodage RelayState)
  - Correction d'une 'NullPointerException' lors de l'appel en CallBack du StepUp

## [2.0.18]

### Added
  - [WSGAPI-96] - Intégration de la vérification du cache distribué dans le healthcheck
  
### Changed

### Fixed
  - [WSGAPI-97] - Relais du code établissement présent dans la session si non retourné par ICG
  - Relais du claim 'opsId' dans une négociation StepUp (encodage/décodage RelayState)
  - Correction d'une 'NullPointerException' lors de l'appel en CallBack du StepUp

## [2.0.17]

### Added
- Ajout des certificats publiques des consommateurs PFS (IT-CE) pour la validation de la signature des JWTs que ce client forge, utilisé pour la consommation d'APIs Natixis

### Changed

### Fixed
- Fix endpoint '/info/apps' pour supprimer la limite de 20 sur le nombre de résultats en sortie

## [2.0.16]

### Added
 - Ajout endpoint '/info/apis' permettant de lister les APIs (nom, version, basePath, organisations, policy de routage, applications consommant l'API avec les scopes)
 - Ajout endpoint '/info/apps' permettant de lister les applications consommatrices (nom, organisation, IDP, flow OAuth, scopes, credentials, APIs consommées)
 - Ajout des issuers DEI_*****_PART, DEI_*****_PRO, DEI_*****_SP, BANQUEENLIGNE_*** et CYBERPLUS_* pour tous les environnements

### Fixed
 - Correction d'un bug provoquant des crashs d'instances dans la fonction removeSamlSubjectConfirmationElements du script Prepare AuthNHint
 - Correctif StepUp (calcul du path depuis backend provider)
 - Correction de Remote Connection pour utiliser le selecteur dynamique de proxies

## [2.0.15]

### Added
 - 89C3 Features : Ajout du paramètre sbx_tppid pour le TRY IT - repris dans le claim psd2 (tppid) du jeton aval
 - 89C3 Features : Correctif mineur de valorisation d'attributs interne policy AS
 - Jeton aval pour DSP2 : renommage du claim "scopes" en "scope" pour respecter la spécification jtw V3
 - Jeton aval pour DSP2 : suppression du scope "extended_transaction_history" (du claim "scope" du jeton aval) lors de l'utilisation d'un refresh token

### Fixed
 - [WSGAPI-91] - IDP EDP: correction de la configuration de l'IDP

## [2.0.14]

### Fixed
 - [WSGAPI-90] - Correction valorisation header HTTP "Host" pour les WSP

## [2.0.13]

### Added
 - [WSGAPI-69] - Ajout IDP (Prospects EDP)

## [2.0.12]

### Changed
- Modification URL (endpoint) ICG à appeler selon le IDP configuré (CYBERPLUS vs autres)
- Routage: optimisation pour la récupération du resourcePath (Backend  API) via policy Bound Routing
- Modification du path /api/oauth/v1/revoke pour alignement V2
- Appels inter services: modification du message d'erreur retourné en cas de conflit
- Suppression certificat ICG DUA inutilisé et Certificat ICG BP et CE expirant en Juillet 2018
- Contrôle du format de l'assertion JWT Bearer
- [WSGAPI-74] - Refresh Token: regénération de l'ID token sans renouvellement

### Fixed
- (ICGBAD) Correction de l'URL ICG à appeler dans le mécanisme de StepUP
- Correction utilisation de l'attribut runtime api.id en lieu et place de l'attribut "Binder"
- Correction CSMP service /revoke

## [2.0.11]

### Changed
 - [Jeton aval V3] Ajout du claim origin lors des appels inter APIs
 - Rajout claim 'scopes' DSP2 dans Jeton Aval

### Fixed
 - PROD_89C3_SANDBOX: correction 'alias' du certificat de signature des JWT

## [2.0.10]

### Fixed
 - Correction du déploiement des policies: la phase de mise à jour des tables KPS était ignorée

## [2.0.9]

### Added
- [WSGAPI-52] - Revocation de l'access token sans révocation du refreshtoken
- [OPENAPI-332] - Ajout de la fonctionnalité TRY-It avec identité de l'utilisateur (pour les besoins 89C3API)
- Ajout certificats PROD_89C3_SANDBOX

### Changed
- Prise en compte du champ UNSECURE au niveau de la servlet d'import des applications consommatrices
- Passage du Code HTTP Status de 400 à 500 lors d'un Mauvais formatage de la Backend URL
- Surcharge Policy /healthcheck ajout prise en compte paramètre delay dans la querystring  pour ajouter un délai sur la réponse en ms (ex : /healthcheck?delay=100)
- Uniformisation de l'authentification sur /api/oauth/token et /api/oauth/revoke
- Webapi Extensions Tools (1.0.19): Prise en compte champ unsecure pour la derogation du client secret + ajout gestion RequestPolicy et ResponsePolicy

### Fixed
- Correction pour DEI du script Redirect to PAuth qui utilise le contexte /dei/login au lieu de /idpas
- [ENVIR-195] - Correction message d'erreur lors de l'appel d'API avec un token expiré
- Correction d'un bug sur la gestion d'erreur StepUp
- Correction Bug sur signature HS256 en authentification d'application

## [2.0.8]

### Added
- Ajout Flag application pour activation client_secret en URL (Dérogation à la RFC 6749 à la demande de Natixis / PPG ABS & BIP )

## [2.0.7]

### Added
- [PFAPIG-550] Flow Inter APIs: implémentation transmission d'identité X-Upstream-JWT-Token (scénario 4)

### Changed
- Modification de la taille max d'une transaction

### Fixed
- Modification de la gestion d'erreur quand le Backend_URL ne dispose pas du bon format
- Ajouts propriétés de lookup sur routing_audience_exeption (KPS)
- Correction URN WSBAD et DEI pour DEV- 

## [2.0.6]

### Added
- Ajout policy Downgrade signature pour ADFS


## [2.0.5]

### Fixed
- Récupération du BPCE-ProspectId pour les IDP PROSPECT

## [2.0.4]

### Fixed
- Correction policy CreationJetAval-SupJetOauth-Inclusion lorsque le hash est disabled


## [2.0.3]

### Added
- Ajout des binding HTTP-Redirect et HTTP-Post pour les endpoints ICG en Anonyme (tout environnement)
- Ajout de l'issuer ICG Proxy en Anonyme (tout environnement)

### Changed
- Force RSA 256 en signature des requêtes SAML pour l'IDP ADFS BP qui ne supporte pas le RSA 512

## [2.0.2]

### Added
- Ajout des paths /api/oauth/v1/revoke et /api/oauth/v2/revoke

## [2.0.1]

### Fixed
- Rétablissement des claims auth_time et sess dans le jeton aval

## [2.0.0]

### Added
 - Automatisation de la migration des tables lors du déploiement
 - Filtrage des scopes opentid
 - StepUp ICGBAD pour DEI
 - Claims DEI dans le jeton aval
 - (*Nouveauté v2) Récupération de l'authentification utilisateur sur un service dédié
 - (*Nouveauté v2) Gestion de la session de consentement OAuth sur un service dédié
 - (*Nouveauté v2) Echange du paramètre RelayState contenant la sauvegarde de la session OAuth (stateless sur flux Redirect avec RelayState)
 - (*Nouveauté v2) Implémentation des bindings SAML Artifact/Redirect/POST-SimpleSign (en plus de SOAP et POST)
 - (*Nouveauté v2) Suppression de redirection sur le service de consentement si celui-ci n'est pas nécessaire

### Changed
 - Implémentation routage V4
 - Enrichissement des messages d'erreur pour routage V4
 - Ajout False Filter dans JSON Fault pour sortie sur erreur et arrêt traitement 
 - Modification de la policy KPS Management (Policy de validation de schéma factorisée)
 - Modification de la valorisation Force Host Header WSP avec nouvel attribut http.request.sni
 - Phase de déploiement: ajout de logs supplémentaires lors de la mise à jour des tables KPS
 - Modification du oauth.downstream.issuer pour flow client Credentials (Filtre Retrieve Downstream Context)
 - ZDD (Zero Downtime Deployment): environnementalisation des variables (valeurs à positionner dans les dictionnaires)
 - Gestion d'erreur en cas d'IDP non identifié
 - Mise en conformité du service OAuth v1 avec la norme RFC6749
 - Alignement de la durée de validité de l'Access Token sur sessions OpenID sans scope 'offline_access'
 - Implémentation PKCE (RFC 7636) pour les Flux Authorization Code
 - Prise en compte du scope 'offline_access' (Interdiction sur les flux 'client_credentials'),
 - Interdiction des scopes non consentis sur '/token'
 - Interdiction du scope 'openid' sur les flux '/token'
 - Prise en charge du paramètre 'id_token_hint' sur le service '/authorize'
 - Prise en charge (et application sur la requête SAML) du paramètre 'prompt' (si prompt présent et ne contiens pas 'login' requête SAML Passive)
 - Prise en compte du mode passif SAML sur les IDP locaux (DEI)
 - Prise en compte des dates d'authentification et d'expiration de session,
 - Prise en compte des dates 'not before' et 'issue time' pour la session utilisateur,
 - Négociation de contenu globale suivant le flux OAuth et le binding SAML (retour 406),
 - Analyse de contenu entrant (retour 415),
 - Filtrage des méthodes (retour 405),
 - Filtrage des entêtes en retour (Date/Cache-Control/Pragma et Vary si service /authorize),
 - Analyse du certificat signataire des messages et assertions SAML,
 - Prise en compte du chiffrement par défaut pour les assertions SAML,
 - Implémentation stricte de la signature SAML,
 - Implémentation stricte du chiffrement SAML
 - Changements des algorithmes par défaut pour la signature et le chiffrement (SHA-512)
 - Gestion de la session StepUp avec des échanges JWE (plus de cache ou de KPS RS/AS)
 - Implémentation du timeout d'authentification StepUp
 - Implémentation de la détection de rejeu sur les échanges StepUp (Cache synchrone RS uniquement)
 - Rajout des services/méthodes/subject sur les appels OAuth (monitoring)
 - Prise en compte des attributs supplémentaires DEI
 - Implémentation et prise en compte du paramètre OAuth 'request' (passage des paramètres avec une requête JWT)
 - Implémentation et prise en compte du paramètre OAuth 'response_mode'
 - Implémentation et prise en compte du paramètre OAuth 'claims'


### Fixed
 - ZDD: le healthchecklb renvoie désormais correctement le statut HTTP 503 lorsqu'il y a un déploiement ou un arrêt d'une instance

## [1.0.85.1]

### Added
 - Ajout des certificats pour l'environnement HOM_89C3

## [1.0.85]

### Changed
- Modification du cache Quota pour passer en mode ASYNCHRONE

## [1.0.84]

### Fixed
 - [quota api] correctif d'un probleme de conversion string/int lors de l'evaluation de depassement de quota

## [1.0.83]

### Added
 - [BAPI-169] Ajout servlet d'export des API et des applications

## [1.0.82]

### Changed
 - [WSGAPI-46] Environnementalisation de la Configuration du mécanisme de réplication STEPUP (False : Réplication par EHCache & True : Réplication par KPS)

## [1.0.81]

### Changed
- [WSGAPI-46] Ajout d'une table KPS pour gestion des sessions StepUp
- [WSGAPI-46] Implémentation des sessions StepUp via KPS

## [1.0.80]

### Changed
- Tables KPS : Modification du type de la colonne 'max_tps' en String

## [1.0.79]

### Changed
- AppImportServlet (1.0.14): Passage de l'attribut overrideOauthCredentials en paramètre de la servlet

### Fixed
- [WSGAPI-43] - Correction erreur de mise à jour table de routage si utilisation du quota

## [1.0.78]

### Changed
 - [WSGAPI-38] - Mise en place d’un appel via Proxy pour les callbacks StepUp

## [1.0.77]

### Changed
 - HOM: Mise à jour données table KPS InstanceConfig suite au déplacement de biliwsg001/002 en dom103

### Fixed
 - RMOE: import nouveau certificat incluant le DNS pour l'AS Métier (biz)

## [1.0.76]

### Added
 - Ajout du scope "wssocket_access" dans la liste blanche de la fonctionnalité WebSocket

### Fixed
- Correction de la recherche de http://BPCE/BPCE-TraceId dans les assertions SAML retournées par les ADFS CE et BP 

## [1.0.75]

### Changed
 - Mise à jour du script de déploiement des policies
    - Ne pas tenir compte de certains messages d'erreurs liés aux APIS mal configurées (suite Upgrade SP6)

## [1.0.74]

### Changed
- Alimentation du claim sub avec BPCE-TraceId pour les ADFS BP et CE
- Désactivation du mot de passe obligatoire lors de l'export des .dat d'APIs et d'appliations dans API Manager

## [1.0.73]

### Fixed
 - Correction de la sélection des proxies

## [1.0.72]

### Added
- Ajout de la possibilité de passer le token en Query String (WebSocket Only)

## [1.0.71]

### Added
- Ajout de la feature WebSocket pour le projet CHATBOT avec gestion de la sécurité OAUTH & JWT Aval
- Enrichissement des erreurs (ErrorTemplates) pour la gestion de la fonctionnalité WebSocket

### Changed
- Variabilisation des libellés des noms de proxies

## [1.0.70]

### Fixed
- Correction des urls de l'IDP ICGBAD pour HOM et PROD  
- Correction des noms et valeurs de variables pour le proxy DMZ Antares


## [1.0.69]

### Fixed
- Ajout du port cible dans le binding des remoteHost pour gérer la réentrance des AS et "OAUth Server"

## [1.0.68]

### Fixed
- SwagImportServlet : Correction anomalie sur la gestion des profils de sécurité

## [1.0.67]

### Fixed
- Correction erreur sur la déclaration du remoteHost pour gérer la réentrance des AS

## [1.0.66]

### Added
- Ajout d'un remoteHost pour gérer la réentrance des AS

### Changed
- Réorganisation des propriétés relatives au rôle Service Provide de la Gateway et aux IDPs
- Politique de réplication des caches : retour vers le mode synchrone

## [1.0.65]

### Changed
- Modification des IDP Prospect : différenciation CE, BP & FC
- Modification de la politique de réplication des caches : passage en asynchrone
- Suppression des vieilles tables KPS et normalisation des noms de tables

## [1.0.64]

### Fixed
 - Correction anomalie sur le script de vérification des statuts des instances lors du déploiement

## [1.0.63]

### Added
- Ajout du remoteObjectPort dans la configuration générale des caches

### Changed
- Ajout étape de vérification des statuts des instances lors du déploiement

### Fixed
- SwagImportServlet : correction anomalie 'connection refused' survenue sur les plateformes auth & ano 

## [1.0.62]

### Added
- [ENVIR 115] Ajout de la gestion des scopes en mode mixte (désactivé)
- Ajout du certificat publique d'Homologation pour la vérification de la signature des jetons forgés par les ROA dans la cinématique StepUp AF

### Changed
- Modification de l'issuer dans les AuthNRequest SAML

### Fixed
- Correction de l'encodage de l'URL 'error_uri'

## [1.0.61]

### Added
- Ajout des certificats ACE et ACI Prod et Hors Prod manquants(BPCE, BP et BPCE IT)

## [1.0.60]

### Added
 - AppImportServlet : 1ère version stable utilisable par la Team Indus pour l'import des applications consommatrices

### Fixed
 - SwagImportServlet : Fix "Connection refused" avec adresse en bbg
 - Gestion correcte du MediaType 'application/hal+json',
 - Gestion de la propriété 'href' du claim 'error_uri'

## [1.0.59]

### Added
 - Ajout de l'ADFS BP GDC (IDPID + certificats) pour la réalisation des livrables côté i-BP

### Changed
 - SwagImportServlet (1.0.2) : 1ère version stable utilisable par la Team Indus pour les imports de swagger
 - Gestion dynamique des proxies pour reduire le nombre de Connect to URL, consommateur de mémoire

### Fixed
 - Suppression des exception sur l'authentification des utilisateurs dans le Service OAuth

## [1.0.58]

### Fixed
 - Mise à jour script déploiement des policies (ne pas tenir compte des erreurs sur les APIs mal configurées dans API Manager)

## [1.0.57]

### Added
 - Ajout des certificats publiques de RMOE et RMOA pour la vérification de la signatures des jetons forgés par les ROA dans la cinématique StepUp AF

### Changed
 - [PFAPIG-524] Changement de la casse du service WSBAD (WSBAD.asmx -> WsBad.asmx)

### Fixed
 - Prise en compte des certificats non exportés pour la récupération par X5T

## [1.0.56]

### Changed
 - [PFAPIG-520] Policy: Code retour positionne à 429 en cas de depassement de quota
 - Package XLD: déplacement des extensions et modules dans les répertoires des instances

## [1.0.55]

### Added
 - Prise en compte des nom de methode pour les callback StepUp

### Changed
 - Modification de la Policy JSON Fault pour sortie en Failure

 
## [1.0.54]

### Fixed
 - Correction bug sur gestion de la chaine de montée d'authentification
 - Correction bug introduit dans précédent commit (préfix fournisseur StepUp)

## [1.0.53]

### Fixed
 - Correction bug introduit sur gestion des callbacks

## [1.0.52]

### Fixed
 - Création d'un attribut dédié pour sauvegarder le prefixe du fournisseur BackEnd.

## [1.0.51]

### Added
 - Activation EHCache pour la gestion des consentements via le cache Local "KPS_Consent_BPCE-IT"
 - Ajout Cache "KPS_Identity_BPCE-IT" pour utilisation sur la table de configuration des IDP
 - Activation du cache sur la collection KPS "OAuth SAML"
 - Ajout du nettoyage des caches lors des opérations DELETE/UPDATE/INSERT dans les tables KPS via l'API
 - Prise en compte du préfixe fournisseur pour les callbacks de StepUp

### Changed
 - Utilisation de la méthode BIND pour la gestion de purge des caches

## [1.0.50]

### Added
 - Ajout des IDP ADFS_BITv3 et ADFS_BITv4
 - Import des certificats de signatures et de chiffrement des assertions SAML pour l'ensemble des ADFS BP & BPCE-IT (BIT)
 
### Changed
 - Renommage ADFS_BAD en ADFS_CE + Configuration des appels AuthNRequest pour les nouveaux IDP ADFS_xx
 - Renommage IDPUTI01 en ADFSCE
 - Correction des idpname pour ADFSCE en DUA, VFO, QPA, PROD ...
 - Mise à jour servlet d'import des swaggers

### Fixed
 - Correction de la prise en compte des entêtes de callback StepUp
 - Renouvellement du certificat AS/RS d'Homologation Anonyme suite à une erreur de frappe dans le CN

## [1.0.49]

### Added
 - Ajout de la gestion des proxies pour les accès DEI et WSBAD 

## [1.0.47]

### Added
 - Ajout d'une policy pour vider le cache KPS_collection et KPS_exception des tables de routage
 - Ajout d'un cache distribué SAML_Artifacts
 - Rajout du support ROP pour IDP Cyber
 - Implémentation erreur StepUp Synchrone

### Changed
 - Mise à jour du service de StepUp:
   - Mise en place de la valeur 'deny' dans l'entête 'X-StepUp-Status' lors de l'appel au denied path
   - Prise en compte de l'entête X-StepUp-ID lors de la semande de StepUp,
   - Changement du message 'invalid zip claim' vers 'invalid_zip_claim'
 - [PFAPIG-440] gestion des quotas, suppression des quotas codeEtab car non specifie
 - Utilisation de la VIP de l'AS Interne pour tous les appels réentrant sur les AS
 - Suppression du contrôle du CN sur les certificats Serveur des backends (temporaire)
 - miscommit... rétablissement du response_type none

## [1.0.46]

### Fixed
 - Correction anomalie sur la gestion d'erreurs StepUP

## [1.0.45]

### Changed
 - [PFAPIG-490] - Ajout gestion d'erreurs StepUP

## [1.0.44]

### Added
 -  Ajout PROSPECT_FC 

### Fixed
 - Modification gestion des IDP Prospect BP et CE

## [1.0.43]

### Changed
 - Mise à jour de la négotiation de contenu: le service '/api/oauth/authorize' renvoie par défaut une réponse au format 'text/html'

### Fixed
 - Mise en commentaire de la valorisation des attributs saml.request.binding & saml.response.binding dans "Scripting Language : Setup OAuth SAML Selectors" 
 - Correction SP4


## [1.0.42]

### Added
 - Ajout de la gestion des quotas

### Changed
 - Adaptation du service d'import des données de routage (modification schema JSON)
 - Modification des filtres de récupération du code établissement pour que l'absence de code ne soit plus tracée comme une erreur
 - Modification du contexte ICG pour PROSPECT_CE : PROSPECT_CE_99999 devient PROSPECT_CE_88888 suite changement côté ICG

### Fixed
 - [ENVIR-145] - Màj Policy Global Fault pour gérer les erreurs d'authentification

## [1.0.41]

### Added
 - Ajout des certificats de signature et chiffrements de l'ADFS CE des environnements DUA, VFO, UTI, QPA et Production utilisés pour les assertions dans les réponses SAML
 - Ajout du SDK pour le développement des policies

### Changed
 - Mise à jour du service d'exposition des certificats (x5t) en utilisant le SDK
 - XLDeploy : mise à jour du package et processus de déploiement (SDK)

## [1.0.40]

### Fixed
- MAJ de l'URL ICG de l'IDP ICGBAD pour pointer vers VFO en DEV au lieu de UTI
- Correction du nom de la variable du certificat signataire JWT et SAML pour les environnements PROD_ANO et PRE_ANO

## [1.0.39]

### Fixed
 - XLDeploy : correction régression sur la création du .dar

## [1.0.38]

### Added
- Ajout de la gestion d'erreur OAuth pour le service oauth/token
- Ajout de la gestion d'erreur OAuth pour le service oauth/authorize
- Ajout du certificat ADFS CE (signature des assertions SAML)

### Changed
- Correctif des attributs à detination de Kibana dans Event Log : FQDN & CHANNEL
- Suppression de l'unité "ms" dans la restitution pour Kibana du temps de traitement du filtre "Connect To URL"

### Fixed

## [1.0.37]

### Fixed
- Correctif extraction Code Etablissement en StepUp
- Correctif extraction OpsId en StepUp

## [1.0.36]
### Fixed
- Mise à jour Certificat IDP (Assertion Recipient)

## [1.0.35]

### Fixed
- Modification des Sélecteurs IDP Gateway (Response Signer, Assertion Signer)

## [1.0.34]

### Added
- Extraction dacsId, authLevel et opsId dans requête SAML
- Ajout de propriété dans le template du jeton Aval (non valorisées):
    - saml.request.opsId : OpsId disponible dans la requête SAML
    - dacs.response.dacsId : ID de la réponse SAML en provenance du DACS
    - dacs.response.level : Niveau de sécurité DACS
- Ajout d'un Remote Host pour désactiver la vérification du certificat client sur l'appel du backend WSP en Production

### Changed

- Mise à jour des certificats pour les instances de Recettes Anonyme avec les nouvelles URLs (définies le 27/11/207)
- Suppression des Follow Redirect dans les Connect to URL sauf pour V2_Default_Provider et V2_WSP_Provider.
- Renouvellement des certificats Authentifiés Recettes, Homologation et Production pour la prise en compte des alias COPE


## [1.0.33]

### Added
  - Ajout des IDP de stepup pour WSBAD

### Changed
  - Mise à jour du script de déploiement de policies pour permettre la désactivation du backup Cassandra
  - Ajout des alias de certificats pour tous les IDPs dans le fichier .csv
  - Modification du fichier OAuthIdentityProviders.POST.csv : ajout des idp PROSPECT_xy pour les infras ANONYME
  - Mise à jour du certificat 'server' pour API Gateway et API Manager d'Homologation


## [1.0.32]

### Fixed
 - Correction de la réplication des caches de sessions d'authentification SAML sur les appels Authorize
    - Ajout de la policy Main Authorize Request
    - Utilisation des filtres de cache
    - Mise en cache en fin de traitement plutôt qu'à la création de la session

## [1.0.31]

### Added
 - Ajout de SwagImport Servlet 3.0.0
 - Ajout d'un cache SAML_Auth_Sessions

### Changed
 - [ENVIR-21] Modification de la gestion des scopes pour tous les flows oauth /authorize et /token
     - Ajout d'un cache SAML_Auth_Sessions
     - Modification du script dans la policy OAuth SAML Selector : utilisation du CacheContainer au lieu du SessionManager pour permettre la réplication
     - ajout d'un flag pour désactivation de la fonctionnalité de check des scopes. Desactivation du check.
     - Ajout d'un filter Remove Attribute pour supprimer l'issuer existant dans la policy AuthNRequest Message d'ICGIBP

### Fixed
 - Correction de la réplication des caches
 - Correction du script de mise en place des extensions ICG

## [1.0.30]

### Added
  - Ajout du certificat pour le chiffrement des assertions SAML pour l'homologation
  - Création d'un cache distribué pour les sessions StepUp
  - Ajout Collection & Table KPS de gestion des consentements OAuth2
  - Ajout JSON Schema de validation nouvelle table de consentements
  - Ajout librairie patch de consentement webapi-extensions-consent-oauth-1.0.0.jar
  - Ajout gestion alimentation nouvelle table

### Changed
  - Mise à jour du Schéma de la table Identity Providers
  - Mise à jour du certificat pour le chiffrement des assertions SAML pour la production
  - Ajout de la vérification de signature de la réponse SAML
  - Suppression des JSON Schema de validation concernant le routage V1
  - Suppression des Policies de routage V1
  - Suppression de la Policy de production de jeton V1
  - Nettoyage Policies Management KPS 

### Fixed
  - Ajout False Filter on Policy Healthcheck (Mode Maintenance)
  - XLDeploy: suppression/copie des ressources serveur par serveur afin d'éviter les risques d'indisponibilité
  - Correction script de déploiement (meilleur parsing du log de déploiement du .fed)

## [1.0.29]

### Fixed
 - Réactivation du 'Follow Redirect' sur le 'Connect to URL' pour l'appel des backends
 - Correction script de déploiement des policies

## [1.0.28]

### Fixed
 - Modification de la policy V2_Main_Routing pour isoler le traitement spécifique du header HOST
 
        -> Preserve HOST Header for WSP
        -> Generate new HOST Header for the others

## [1.0.27]

### Fixed
 - Mise à jour du script de déploiement des policies sur la prise en compte des 'Path Conflict' dans API Manager

## [1.0.26]

### Changed
- [ENVIR-140] - Modification du script Compute Target Path dans la policy V2_Main_Routing pour gérer la présence inopportune d'un / à la fin d'une méthode d'une API

### Fixed
- BugFix: Récupération cdetab sur flux ClientCredentials

## [1.0.25]

### Fixed
Correctifs AS StepUp:
 - PROSPECT_BP CDETAB fixe à 99999
 - JSON Path Context aval facultatif (client credentials)
 - Appel Saml Bindings dans SAML Library (fix Q'n'D flux ROP)
 - Correction de la génération des AuthNRequest pour les services AccessToken 
 - Appel du script Saml Bindings dans policy de génération AuthNRequest dans tous les cas

## [1.0.24]

### Added
 - Ajout des OAuth IDP pour l'instance SUPPORT
 - Import Initial StepUp

### Changed
- Suppression de policies inutilisées (Hors container BPCE IT)
- Mise à jour Access Token Service
- Cleanup Scripts de routage
- Correction script Jeton Aval

### Fixed

## [1.0.23]

### Fixed
 - Correction de la valeur de 'ValueType' lors de l'assertion SOAP Binary

## [1.0.22]

### Added
 - Ajout configuration pour permettre la connexion des utilisateurs déclarés dans un AD
 - Ajout de tests step dans les tests SOAP UI error_template

### Fixed
 - Renommage du claim iat en nbf dans le script Build Downstream Token de la policy 'V2_CreationJetAval-SupJetOauth-Inclusion

## [1.0.21]

### Fixed
 - Correction contrat d'interface pour la récupération de jetons JWT

## [1.0.20]

### Changed
 - Suppression des Policies de routage V1 dans Server Settings (Routing Policies) - Plus visible d'API Manager. Les policies seront supprimées dans un deuxième temps
 - Force Preserve Host on Connect To URL (Backend)
 - Force Host Header for WSP with ${env.update.resource.server.name}

### Fixed
 - Retour arriere sur la Validation Client Credentials

## [1.0.19]

### Fixed
 - Correction régression sur l'environnementalisation du facteur de réplication de Cassandra

## [1.0.18]

### Added
 - Ajout des certificats pour l'instance SUPPORT
 - Ajout Nouveau Custom Attribut cs.ProcessingTimeinMS --> Log Event 
 - Ajout des certificats ICG de signature des réponses SAML pour BP, CE, DUA, IRQ et UTI. (QPA & VFO déjà présents)
 - Ajout de codes manquants dans le fichier ErrorTemplates.POST.csv : ERROAUTH_BADRESPONSE, ERROAUTH_INVALIDCREDENTIALS, ERROAUTH_INVALIDRESPONSE, ERROAUTH_MISSINGASSERTIONAUTHTIME, ERROAUTH_MISSINGASSERTIONISSUER, ERROAUTH_MISSINGPROVIDEREXTRACTION, ERROAUTH_NORESPONSEREFERRER, ERROAUTH_NOTACCEPTABLE, ERROAUTH_WEBSSONOTAVAILABLE


### Changed
 - Modification des alias de certificats de signature & déplacement de certificats de WEBAPI_Projects vers WEBAPI_settings
 - Modification du nom de variables et commentaires pour le certificat de signature
 - Sécurisation et environnementalisation de l'accès à la base Cassandra via un user / password
 - Environnementalisation du niveau de log des instances
 - Modification du pattern de validation des autorités DEI
 - Changement du mode de génération du Jeton Aval (template dans méta-données AT).
 - Vérification de la signature de l'assertion (confiance)
 - Rajout d'une option pour désactiver le filtrage des flux OAuth
 - Deplacement SAML ICG Assertion Encrypt vers WEBAPI_Server_Settings
 - [ENVIR-21] Modification de la gestion des scopes dans la génération des tokens oauth
 - oauth token: suppression des meta-données non utilisées

### Fixed
 - Correction du script Retrieve Resource path from backend dans la policy V2_Main_Routing (Ajout attribut "api.method.path")
 - Correction de la prise en compte des paramètres dans le path du type : /monchemin/{myparam}

## [1.0.17]

### Changed
 - Ajout des certificats AS-RS Authentifié Production

### Fixed
 - Correction du script Retrieve Resource path from backend dans la policy V2_Main_Routing
 - Correction régression sur les scripts shell de déploiements qui n'étaient plus exécutables
 - Correction de l'url ICG pour l'idp Cyberplus en Homologation


## [1.0.16]

### Changed
 -  Ajout d'un contournement afin d'écarter les resources Paths renseignés à tort par les développeurs (Backend)
 - [PFAPIG-385] - Personnalisation des logs

## [1.0.14]

### Added
    
 - [PFAPIG-411]

        Ajouts Policies d'outillage de routing management.
        Permet d'extraire le contenu complet de chacune des tables au fomat JSON avec la résolution de l'URL Backend
        Permet de simuler le routage API, Méthode, Exception en fournissant les données api_name, api_method, code établissement ....
        
 - [PFAPIG-412] - Optimisation par Gestion de cache du routage par exception
 - Ajout des AC TEST GCE Authentification V2 et TEST GCE Racine nécessaire pour l'accès à DEI

### Changed

 - Modification du proxy à utiliser pour l'appel à DEI
 - Modification des idpspaces pour les lignes MOA i-BP
 - Modification de la policy V2_CreationJetAval-SupJetOauth-Inclusion pour gérer le cas d'inexistence du backend provider + ajout du template d'erreur ERRCOM_ROU003

### Fixed

 - Correction de la policy Default JWT Signer - fix du filtre Find Certificate bis : désenvironnementalisation !

## [1.0.13]

### Fixed
 - [ENVIR-82] - Correction des certificats

## [1.0.11]

### Changed

 - Mise en oeuvre du certificat de signature des JWT fournisseur (Temporairement, certificat R7 DEV sur tous les environnements hors Prod)
 - Renouvellement des certificats avec changement de signataire, AC BPCE Hors Prod
 - [PFAPIG-387] 
                
                Modification de la Policy ICGIBP pour prendre en compte les lignes de MAP iBP - Ajout des idp associés (codes environnements différents)
                Modification de ICGIBP > AuthNRequest Message pour gérer les cas d'Homologation et Production
                Ajout du DEI Proxy et modification de la policy Validate DEI Cookie pour l'utiliser dans le filtre Connect to URL
                Environnementalisation du proxy bunker pour DEI + Suppression de la variable d'environnement env.port.BROKER inutilisée

## [1.0.10]

### Fixed
 - [ENVIR-44] - Correction de la policy BPCE-IT Resource Owner Password
 - Mise à jour du HashJacksonScript : méthode update0 - if query is empty query = null
 - Correction de la policy Saml Authorization Request
 - Ajout des AC TEST BPCE UCG ACI GROUPE et TEST BPCE UCG ACE MATERIELS GROUPE dans le truststore
 - Correction des assertions sur ERRFLW_MSG001 dans les tests soap UI
 - Ajout des tests SOAP UI pour la gestion des erreurs de la validation de l'authentification client (ERRCOMM_FLW***)

## [1.0.9]

### Fixed
 -  Correctif sur la policy Oauth Flow Fault - erreur de syntaxe dans Evaluate Selector : exist oauth.app.error.code
 -  Correctif sur la policy SAML Authorization Request : gestion des erreurs de la validation de l'authentification client (application)

## [1.0.8]

### Added
 - Gestion des erreurs de la Validation Client (Application)

### Changed
 - [ENVIR-11] - Gestion du client id erroné : retour d'une erreur HTTP 401

## [1.0.7]

### Added
 - [PFAPIG-314] Réécriture d'url pour API des applications éditeurs externes (Virtualisation des Resources Path FrontEnd/Backend)
 - [PFAPIG-352] Niveau d'authentification des applications
 - Restriction SSL lors du routage sur les Backends (Utilisation TLSv1.1 & TLSv1.2 only)

### Changed
 - [PFAPIG-324] Ajout du jeton aval dans l'en-tête X-Downstream-JWT-Token en complément de l'entête Authorization : Bearer qui doit être déprécié. Une fois que nous nous serons assurés que cet en-tête n'est plus utilisé, il pourra être désactivé en modifiant le paramètre ENV_UPDATE_RESOURCE_SERVER_JWT_TOKEN_CUSTOM_HEADER_ONLY de TRUE à FALSE. Ainsi nous permettons la gestion de la transition avec le minimum d'impact.

### Fixed
 - [ENVIR-33] Correction Apportée sur Code retour --> Force HTTP STATUS 500 En lieu et place de l'attribut http.response.status du connect to URL retournant 200 sur pb backend
 - [PFAPIG-357] Modification du script HashJacksonScript : méthode update0, le verbe HTTP ne doit pas etre null si la queryString est vide.

## [1.0.6]

### Added
- OAuth2
 - Détection du niveau de sécurité de l'authentification des Applications
 - Mise en place des IdP PROSPECT\_BP et PROSPECT\_CE
- Nouvelle Policy HealthCheckOSE pour utilisation pour la supervision (mécanisme OAUTH utilisateur)

### Changed
- OAuth2
 - Alignement sur service de contrôle jeton DEI
 - Modification des expressions JSON Path pour récupération des claims DEI,
 - Changement de l'URI du service DEI 'Controler Jeton'
 - Normalisation URI WSBAD
- Modiciation Policy HealthCheckLB pour utilisation par les F5 (Suppression génération JETON)
- Ajout mécanisme de gestion de mise en maintenance manuelle des instances (Demande A. LATTE)

### Fixed
- PFAPIG-333 : Validation de l'autorité DEI (Tout environnement)

## [1.0.5] - 2017-09-08
### Added
 - Ajout de la validation de l'autorité dans la policy AuthNRequest Message de l'idp DEI
 
### Changed
 - Modification de la source du Jeton Aval de l'en-tête Authorization à X-Downstream-JWT-Token afin de garder la fonctionnalité native de l'entête Authorization.

### Fixed
 - Modification de la génération des claims du jeton aval pour différencier un jeton aval anonyme ou authentifié (les claims non valorisés n'apparaissent pas)
 - Suppression des claims sys et lvl de jeton aval pour être conforme à la spécification du jeton aval


 
## [1.0.4] - 2017-08-24
### Added
 - Ajout des restrictions de flows OAUTH par Application

 
## [1.0.3] - 2017-08-03
### Added
- OAuth2
 - Négociation de contenu sur la requête d’autorisation
 - Génération et validation des conditions de validité SAML
 - Prise en compte du paramètre Code Etablissement pour les IDP ICG
 - Ajout de la prise en compte du cache pour le routage
 - Assertion WSBad
 - Authorization Code / Implicit Grant pour DEI
- HealthCheck - Load Balancer

### Changed
- Refonte des règles de routage incluant la gestion de la version du fournisseur
 - Routage par API et Code Etablissement (suppression du Code SI)
 - Routage par API Méthode et Code Etablissement (suppression du Code SI)
 - Routage par API sans Code Etablissement
 - Routage par API Méthode sans Code Etablissement
 - Routage par exception
 - Ajout de nouvelles tables de routage (ces tables remplaceront à terme les tables existantes)
- Modification du jeton aval JWT
 - Suppression du Code SI
 - Prise en compte de l’algorithme de hash
 - Génération de l’entête X5T (pour identification du certificat de signature)
- OAuth2 - ROP compatible ICG (Application : API_SELFCARE_POC_13807)
 
## [1.0.2] - 2017-07-06

### Changed
- Mise à jour des fichiers de propriétés pour les environnements DEV, RMOE & RMOA

## [1.0.1] - 2017-07-06
### Added
- Authorization Server
 - Flow Ressource Owner
 - Flow Token JWT
 - Flow Token SAML en SOAP (validé avec IDP Local)
 - Ajout du flow Client Credentials
 
### Changed
 - Ressource Server : Routage
  - Intégration de la gestion (génération ou non) du jeton aval dans les policies de routage.
  - Conservation de 2 nouvelles policies de routage (la policy de routage par défaut ne doit plus être utilisée)
  - Prise en compte du code établissement dans le header si celui-ci n'est pas fourni dans l'access token de l'Authorization Server (cas Natixis)
  - Détails des évolutions du routage Cf. Confluence

### Fixed
- Correction de bugs lors de la génération des claims du jeton aval

## 1.0.0 - 2017-06-22
### Added
- Authentification application : flux Client credentials
 - mode d'obtention : header authorization
 - mode d'obtention : querystring
- Authentification utilisateur
sur ICG (Application Cyberplus) via SAML webSSO avec les flux Oauth2 :
 - Authorization code
 - Implicit
- Refresh token
- Gestion du bannissement d’adresses IP
- Routage par:
 - SI/code étab/API
 - SI/code étab/méthode API
- Gestion du jeton aval JWT (v1 du 28/04)
- Gestion du cache HTTP


[Unreleased]: https://xxxxx/v2.13.02...HEAD
[2.13.02]: https://xxxxx/v2.13.01...v2.13.02
[2.13.01]: https://xxxxx/v2.13.00...v2.13.01
[2.13.00]: https://xxxxx/v2.12.02...v2.13.00
[2.12.02]: https://xxxxx/v2.12.01...v2.12.02
[2.12.01]: https://xxxxx/v2.12.00...v2.12.01
[2.12.00]: https://xxxxx/v2.11.09...v2.12.00
[2.11.09]: https://xxxxx/v2.11.08...v2.11.09
[2.11.08]: https://xxxxx/v2.11.07...v2.11.08
[2.11.07]: https://xxxxx/v2.11.06...v2.11.07
[2.11.06]: https://xxxxx/v2.11.05...v2.11.06
[2.11.05]: https://xxxxx/v2.11.04...v2.11.05
[2.11.04]: https://xxxxx/v2.11.03...v2.11.04
[2.11.03]: https://xxxxx/v2.11.02...v2.11.03
[2.11.02]: https://xxxxx/v2.11.01...v2.11.02
[2.11.01]: https://xxxxx/v2.11.00...v2.11.01
[2.11.00]: https://xxxxx/v2.10.09...v2.11.00
[2.10.09]: https://xxxxx/v2.10.08...v2.10.09
[2.10.08]: https://xxxxx/v2.10.07...v2.10.08
[2.10.07]: https://xxxxx/v2.10.06...v2.10.07
[2.10.06]: https://xxxxx/v2.10.05...v2.10.06
[2.10.05]: https://xxxxx/v2.10.04...v2.10.05
[2.10.04]: https://xxxxx/v2.10.03...v2.10.04
[2.10.03]: https://xxxxx/v2.10.02...v2.10.03
[2.10.02]: https://xxxxx/v2.10.01...v2.10.02
[2.10.01]: https://xxxxx/v2.10.00...v2.10.01
[2.10.00]: https://xxxxx/v2.09.04...v2.10.00
[2.09.04]: https://xxxxx/v2.09.03...v2.09.04
[2.09.03]: https://xxxxx/v2.09.02...v2.09.03
[2.09.02]: https://xxxxx/v2.09.01...v2.09.02
[2.09.01]: https://xxxxx/v2.08.03...v2.09.01
[2.09.00]: https://xxxxx/v2.08.03...v2.09.00
[2.08.03]: https://xxxxx/v2.08.02...v2.08.03
[2.08.02]: https://xxxxx/v2.08.01...v2.08.02
[2.08.01]: https://xxxxx/v2.08.00...v2.08.01
[2.08.00]: https://xxxxx/v2.07.06...v2.08.00
[2.07.06]: https://xxxxx/v2.07.05...v2.07.06
[2.07.05]: https://xxxxx/v2.07.04...v2.07.05
[2.07.04]: https://xxxxx/v2.07.03...v2.07.04
[2.07.03]: https://xxxxx/v2.07.01...v2.07.03
[2.07.02]: https://xxxxx/v2.07.01...v2.07.02
[2.07.01]: https://xxxxx/v2.07.00...v2.07.01
[2.07.00]: https://xxxxx/v2.06.04...v2.07.00
[2.06.04]: https://xxxxx/v2.06.03...v2.06.04
[2.06.03]: https://xxxxx/v2.06.02...v2.06.03
[2.06.02]: https://xxxxx/v2.06.01...v2.06.02
[2.06.01]: https://xxxxx/v2.06.00...v2.06.01
[2.06.00]: https://xxxxx/v2.05.17...v2.06.00
[2.05.18]: https://xxxxx/v2.05.17...v2.05.18
[2.05.17]: https://xxxxx/v2.05.16...v2.05.17
[2.05.16]: https://xxxxx/v2.05.15...v2.05.16
[2.05.15]: https://xxxxx/v2.05.14...v2.05.15
[2.05.14]: https://xxxxx/v2.05.13...v2.05.14
[2.05.13]: https://xxxxx/v2.05.12...v2.05.13
[2.05.12]: https://xxxxx/v2.05.11...v2.05.12
[2.05.11]: https://xxxxx/v2.05.10...v2.05.11
[2.05.10]: https://xxxxx/v2.05.09...v2.05.10
[2.05.09]: https://xxxxx/v2.05.08...v2.05.09
[2.05.08]: https://xxxxx/v2.05.07...v2.05.08
[2.05.07]: https://xxxxx/v2.05.06...v2.05.07
[2.05.06]: https://xxxxx/v2.05.05...v2.05.06
[2.05.05]: https://xxxxx/v2.05.04...v2.05.05
[2.05.04]: https://xxxxx/v2.05.03...v2.05.04
[2.05.03]: https://xxxxx/v2.05.02...v2.05.03
[2.05.02]: https://xxxxx/v2.05.01...v2.05.02
[2.05.01]: https://xxxxx/v2.05.00...v2.05.01
[2.05.00]: https://xxxxx/v2.04.10...v2.05.00
[2.04.11]: https://xxxxx/v2.04.10...v2.04.11
[2.04.10]: https://xxxxx/v2.04.09...v2.04.10
[2.04.09]: https://xxxxx/v2.04.08...v2.04.09
[2.04.08]: https://xxxxx/v2.04.07...v2.04.08
[2.04.07]: https://xxxxx/v2.04.06...v2.04.07
[2.04.06]: https://xxxxx/v2.04.05...v2.04.06
[2.04.05]: https://xxxxx/v2.04.04...v2.04.05
[2.04.04]: https://xxxxx/v2.04.03...v2.04.04
[2.04.03]: https://xxxxx/v2.04.02...v2.04.03
[2.04.02]: https://xxxxx/v2.04.01...v2.04.02
[2.04.01]: https://xxxxx/v2.04.00...v2.04.01
[2.04.00]: https://xxxxx/v2.03.15...v2.04.00
[2.03.17]: https://xxxxx/v2.03.16...v2.03.17
[2.03.16]: https://xxxxx/v2.03.15...v2.03.16
[2.03.15]: https://xxxxx/v2.03.14...v2.03.15
[2.03.14]: https://xxxxx/v2.03.13...v2.03.14
[2.03.13]: https://xxxxx/v2.03.12...v2.03.13
[2.03.12]: https://xxxxx/v2.03.11...v2.03.12
[2.03.11]: https://xxxxx/v2.03.10...v2.03.11
[2.03.10]: https://xxxxx/v2.03.09...v2.03.10
[2.03.09]: https://xxxxx/v2.03.07...v2.03.09
[2.03.07]: https://xxxxx/v2.03.06...v2.03.07
[2.03.06]: https://xxxxx/v2.3.5...v2.03.06
[2.3.5]: https://xxxxx/v2.3.4...v2.3.5
[2.3.4]: https://xxxxx/v2.3.3...v2.3.4
[2.3.3]: https://xxxxx/v2.3.2...v2.3.3
[2.3.2]: https://xxxxx/v2.3.1...v2.3.2
[2.3.1]: https://xxxxx/v2.3.0...v2.3.1
[2.3.0]: https://xxxxx/v2.2.10...v2.3.0
[2.2.10]: https://xxxxx/v2.2.9...v2.2.10
[2.2.9]: https://xxxxx/v2.2.8...v2.2.9
[2.2.8]: https://xxxxx/v2.2.7...v2.2.8
[2.2.7]: https://xxxxx/v2.2.6...v2.2.7
[2.2.6]: https://xxxxx/v2.2.5...v2.2.6
[2.2.5]: https://xxxxx/v2.2.4...v2.2.5
[2.2.4]: https://xxxxx/v2.2.3...v2.2.4
[2.2.3]: https://xxxxx/v2.2.2...v2.2.3
[2.2.2]: https://xxxxx/v2.2.1...v2.2.2
[2.2.1]: https://xxxxx/v2.2.0...v2.2.1
[2.2.0]: https://xxxxx/v2.1.5...v2.2.0
[2.1.5]: https://xxxxx/v2.1.4...v2.1.5
[2.1.4]: https://xxxxx/v2.1.3...v2.1.4
[2.1.3]: https://xxxxx/v2.1.2...v2.1.3
[2.1.2]: https://xxxxx/v2.1.1...v2.1.2
[2.1.1]: https://xxxxx/v2.1.0...v2.1.1
[2.1.0]: https://xxxxx/v2.0.39...v2.1.0
[2.0.39]: https://xxxxx/v2.0.38...v2.0.39
[2.0.38]: https://xxxxx/v2.0.37...v2.0.38
[2.0.37]: https://xxxxx/v2.0.36...v2.0.37
[2.0.36]: https://xxxxx/v2.0.35...v2.0.36
[2.0.35]: https://xxxxx/v2.0.34...v2.0.35
[2.0.34]: https://xxxxx/v2.0.33...v2.0.34
[2.0.33]: https://xxxxx/v2.0.32...v2.0.33
[2.0.32]: https://xxxxx/v2.0.31...v2.0.32
[2.0.31]: https://xxxxx/v2.0.30...v2.0.31
[2.0.30]: https://xxxxx/v2.0.29...v2.0.30
[2.0.29]: https://xxxxx/v2.0.28...v2.0.29
[2.0.28]: https://xxxxx/v2.0.27...v2.0.28
[2.0.27]: https://xxxxx/v2.0.26...v2.0.27
[2.0.26]: https://xxxxx/v2.0.25...v2.0.26
[2.0.25]: https://xxxxx/v2.0.24...v2.0.25
[2.0.24]: https://xxxxx/v2.0.23...v2.0.24
[2.0.23]: https://xxxxx/v2.0.22...v2.0.23
[2.0.22]: https://xxxxx/v2.0.21...v2.0.22
[2.0.21]: https://xxxxx/v2.0.20...v2.0.21
[2.0.20]: https://xxxxx/v2.0.19...v2.0.20
[2.0.19]: https://xxxxx/v2.0.18...v2.0.19
[2.0.18]: https://xxxxx/v2.0.17...v2.0.18
[2.0.17]: https://xxxxx/v2.0.16...v2.0.17
[2.0.16]: https://xxxxx/v2.0.15...v2.0.16
[2.0.15]: https://xxxxx/v2.0.14...v2.0.15
[2.0.14]: https://xxxxx/v2.0.13...v2.0.14
[2.0.13]: https://xxxxx/v2.0.12...v2.0.13
[2.0.12]: https://xxxxx/v2.0.11...v2.0.12
[2.0.11]: https://xxxxx/v2.0.10...v2.0.11
[2.0.10]: https://xxxxx/v2.0.9...v2.0.10
[2.0.9]: https://xxxxx/v2.0.8...v2.0.9
[2.0.8]: https://xxxxx/v2.0.7...v2.0.8
[2.0.7]: https://xxxxx/v2.0.6...v2.0.7
[2.0.6]: https://xxxxx/v2.0.5...v2.0.6
[2.0.5]: https://xxxxx/v2.0.4...v2.0.5
[2.0.4]: https://xxxxx/v2.0.3...v2.0.4
[2.0.3]: https://xxxxx/v2.0.2...v2.0.3
[2.0.2]: https://xxxxx/v2.0.1...v2.0.2
[2.0.1]: https://xxxxx/v2.0.0...v2.0.1
[2.0.0]: https://xxxxx/hotfix%2F1.0.85.1...v2.0.0
[1.0.85.1]: https://xxxxx/v1.0.85...hotfix%2F1.0.85.1
[1.0.85]: https://xxxxx/v1.0.84...v1.0.85
[1.0.84]: https://xxxxx/v1.0.83...v1.0.84
[1.0.83]: https://xxxxx/v1.0.82...v1.0.83
[1.0.82]: https://xxxxx/v1.0.81...v1.0.82
[1.0.81]: https://xxxxx/v1.0.80...v1.0.81
[1.0.80]: https://xxxxx/v1.0.79...v1.0.80
[1.0.79]: https://xxxxx/v1.0.78...v1.0.79
[1.0.78]: https://xxxxx/v1.0.77...v1.0.78
[1.0.77]: https://xxxxx/v1.0.76...v1.0.77
[1.0.76]: https://xxxxx/v1.0.75...v1.0.76
[1.0.75]: https://xxxxx/v1.0.74...v1.0.75
[1.0.74]: https://xxxxx/v1.0.73...v1.0.74
[1.0.73]: https://xxxxx/v1.0.72...v1.0.73
[1.0.72]: https://xxxxx/v1.0.71...v1.0.72
[1.0.71]: https://xxxxx/v1.0.70...v1.0.71
[1.0.70]: https://xxxxx/v1.0.69...v1.0.70
[1.0.69]: https://xxxxx/v1.0.68...v1.0.69
[1.0.68]: https://xxxxx/v1.0.67...v1.0.68
[1.0.67]: https://xxxxx/v1.0.66...v1.0.67
[1.0.66]: https://xxxxx/v1.0.65...v1.0.66
[1.0.65]: https://xxxxx/v1.0.64...v1.0.65
[1.0.64]: https://xxxxx/v1.0.63...v1.0.64
[1.0.63]: https://xxxxx/v1.0.62...v1.0.63
[1.0.62]: https://xxxxx/v1.0.61...v1.0.62
[1.0.61]: https://xxxxx/v1.0.60...v1.0.61
[1.0.60]: https://xxxxx/v1.0.59...v1.0.60
[1.0.59]: https://xxxxx/v1.0.58...v1.0.59
[1.0.58]: https://xxxxx/v1.0.57...v1.0.58
[1.0.57]: https://xxxxx/v1.0.56...v1.0.57
[1.0.56]: https://xxxxx/v1.0.55...v1.0.56
[1.0.55]: https://xxxxx/v1.0.54...v1.0.55
[1.0.54]: https://xxxxx/v1.0.53...v1.0.54
[1.0.53]: https://xxxxx/v1.0.52...v1.0.53
[1.0.52]: https://xxxxx/v1.0.51...v1.0.52
[1.0.51]: https://xxxxx/v1.0.50...v1.0.51
[1.0.50]: https://xxxxx/v1.0.49...v1.0.50
[1.0.49]: https://xxxxx/v1.0.47...v1.0.49
[1.0.47]: https://xxxxx/v1.0.46...v1.0.47
[1.0.46]: https://xxxxx/v1.0.45...v1.0.46
[1.0.45]: https://xxxxx/v1.0.44...v1.0.45
[1.0.44]: https://xxxxx/v1.0.43...v1.0.44
[1.0.43]: https://xxxxx/v1.0.42...v1.0.43
[1.0.42]: https://xxxxx/v1.0.41...v1.0.42
[1.0.41]: https://xxxxx/v1.0.40...v1.0.41
[1.0.40]: https://xxxxx/v1.0.39...v1.0.40
[1.0.39]: https://xxxxx/v1.0.38...v1.0.39
[1.0.38]: https://xxxxx/v1.0.37...v1.0.38
[1.0.37]: https://xxxxx/v1.0.36...v1.0.37
[1.0.36]: https://xxxxx/v1.0.35...v1.0.36
[1.0.35]: https://xxxxx/v1.0.34...v1.0.35
[1.0.34]: https://xxxxx/v1.0.33...v1.0.34
[1.0.33]: https://xxxxx/v1.0.32...v1.0.33
[1.0.32]: https://xxxxx/v1.0.31...v1.0.32
[1.0.31]: https://xxxxx/v1.0.30...v1.0.31
[1.0.30]: https://xxxxx/v1.0.29...v1.0.30
[1.0.29]: https://xxxxx/v1.0.28...v1.0.29
[1.0.28]: https://xxxxx/v1.0.27...v1.0.28
[1.0.27]: https://xxxxx/v1.0.26...v1.0.27
[1.0.26]: https://xxxxx/v1.0.25...v1.0.26
[1.0.25]: https://xxxxx/v1.0.24...v1.0.25
[1.0.24]: https://xxxxx/v1.0.23...v1.0.24
[1.0.23]: https://xxxxx/v1.0.22...v1.0.23
[1.0.22]: https://xxxxx/v1.0.21...v1.0.22
[1.0.21]: https://xxxxx/v1.0.20...v1.0.21
[1.0.20]: https://xxxxx/v1.0.19...v1.0.20
[1.0.19]: https://xxxxx/v1.0.18...v1.0.19
[1.0.18]: https://xxxxx/v1.0.17...v1.0.18
[1.0.17]: https://xxxxx/v1.0.16...v1.0.17
[1.0.16]: https://xxxxx/v1.0.14...v1.0.16
[1.0.14]: https://xxxxx/v1.0.13...v1.0.14
[1.0.13]: https://xxxxx/v1.0.11...v1.0.13
[1.0.11]: https://xxxxx/v1.0.10...v1.0.11
[1.0.10]: https://xxxxx/v1.0.9...v1.0.10
[1.0.9]: https://xxxxx/v1.0.8...v1.0.9
[1.0.8]: https://xxxxx/v1.0.7...v1.0.8
[1.0.7]: https://xxxxx/v1.0.6...v1.0.7
[1.0.6]: https://xxxxx/v1.0.5...v1.0.6
[1.0.5]: https://xxxxx/v1.0.4...v1.0.5
[1.0.4]: https://xxxxx/v1.0.3...v1.0.4
[1.0.3]: https://xxxxx/v1.0.2...v1.0.3
[1.0.2]: https://xxxxx/v1.0.1...v1.0.2
[1.0.1]: https://xxxxx/v1.0.0...v1.0.1
